﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Cliente
    {
        public string Nombre { get; private set; }
        public int Dni { get; private set; }
        public CondicionTributaria Condicion { get; private set; }

        public Cliente(string nombre, int dni, CondicionTributaria condicion)
        {
            Nombre = nombre;
            Dni = dni;
            Condicion = condicion;
        }
        
    }

}
