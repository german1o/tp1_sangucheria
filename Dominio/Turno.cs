﻿using System;
using System.Collections.Generic;

namespace Dominio
{
    public class Turno
    {
        public Empleado Empleado { get;}
        public List<Venta> Ventas { get; }

        public Turno(Empleado empleado)
        {
            Empleado = empleado;
            Ventas = new List<Venta>();
        }

        public double Finalizar()
        {
            double total = 0;
            Ventas.ForEach(venta => total += venta.GetTotal());
            return total;
        }
    }
}