﻿namespace Dominio
{
    public class Empleado
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }

        public Empleado(string nombre, string apellido)
        {
            Nombre = nombre;
            Apellido = apellido;
        }
    }
}