﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class PuntoDeVenta
    {
        public int Numero { get; }
        public List<Turno> Turnos { get; }


        public PuntoDeVenta(int numero, List<Turno> turnos)
        {
            Numero = numero;
            Turnos = turnos;
        }

        public void IniciarTurno(Empleado empleado)
        {
            Turno turno = new Turno(empleado);
            Turnos.Add(turno);
        }

        public void AgregarVentaAlTurnoEnCurso(Venta venta)
        {
            ObtenerTurnoEnCurso().Ventas.Add(venta);
        }

        public double FinalizarTurnoEnCurso()
        {
            return ObtenerTurnoEnCurso().Finalizar();
        }

        public Turno ObtenerTurnoEnCurso()
        {
            return Turnos.Last();
        }

        public Empleado ObtenerEmpleadoDelTurnoEnCurso()
        {
            
            return ObtenerTurnoEnCurso().Empleado;
        }
    }
}
