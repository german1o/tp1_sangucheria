﻿using System.Collections.Generic;

namespace Dominio
{
    public class Venta
    {
        public List<LineaDeVenta> Lineas { get; set; }
        public TipoComprobante TipoDeComprobante { get;}
        public int NumeroComprobante { get; set; }
        public Cliente Comprador { get; }
        public int Cae { get; private set; }
        public Venta(Cliente comprador, CondicionTributaria condicionNegocio, int numeroComprobante)
        {
            Lineas = new List<LineaDeVenta>();
            NumeroComprobante = numeroComprobante;
            TipoDeComprobante = condicionNegocio.GetTipoComprobantePara(comprador.Condicion);
            Comprador = comprador;
        }


        public void AgregarLineaDeVenta(LineaDeVenta linea)
        {
            Lineas.Add(linea);
        }

        public void RemoverLineaDeVenta(LineaDeVenta linea)
        {
            Lineas.Remove(linea);
        }

        public void Autorizar(int cae)
        {
            Cae = cae;
        }

        public double GetTotal()
        {
            double total = 0;
            foreach(LineaDeVenta linea in Lineas)
            {
                total += linea.GetTotal();
            }
            return total;
        }



    }
}