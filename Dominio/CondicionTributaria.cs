﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class CondicionTributaria
    {
        public static readonly CondicionTributaria ResponsableInscripto = new CondicionTributaria("Responsable Inscripto");
        public static readonly CondicionTributaria Monotributista = new CondicionTributaria("Monotributista");
        public static readonly CondicionTributaria Exento = new CondicionTributaria("Exento");
        public static readonly CondicionTributaria NoResponsable = new CondicionTributaria("No Responsable");
        public static readonly CondicionTributaria ConsumidorFinal = new CondicionTributaria("ConsumidorFinal");
        public string Condicion { get; }


        public CondicionTributaria(string condicionTributaria)
        {
            Condicion = condicionTributaria;
        }

        public TipoComprobante GetTipoComprobantePara(CondicionTributaria condicionTributaria)
        {
            TipoComprobante tipoComprobante = TipoComprobante.FacturaC;
            switch (Condicion)
            {
                case "Responsable Inscripto":
                    
                    switch (condicionTributaria.Condicion)
                    {
                        case "Responsable Inscripto":
                            tipoComprobante = TipoComprobante.FacturaA;
                            break;

                        case "Monotributista":
                            tipoComprobante = TipoComprobante.FacturaB;
                            break;

                        case "Exento":
                            tipoComprobante = TipoComprobante.FacturaB;
                            break;

                        case "No Responsable":
                            tipoComprobante = TipoComprobante.FacturaB;
                            break;

                        case "Consumidor Final":
                            tipoComprobante = TipoComprobante.FacturaB;
                            break;
   
                    }
                    break;                
            }

            return tipoComprobante;
        }





    }
}
