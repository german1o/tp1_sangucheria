﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Producto
    {
        public int Codigo { get; set; }
        public double Precio { get; set; }
        public string Descripcion { get; set; }
        public int Existencias { get; set; }
        public Rubro Rubro { get; set; }
        public bool SeControlanExistencias { get; set; }

        public Producto(int codigo, double precio, string descripcion, int existencias, Rubro rubro, bool seControlanExistencias)
        {
            Codigo = codigo;
            Precio = precio;
            Descripcion = descripcion;
            Existencias = existencias;
            Rubro = rubro;
            SeControlanExistencias = seControlanExistencias;
        }
    }
}
