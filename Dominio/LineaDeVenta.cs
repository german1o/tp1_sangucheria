﻿using System.Collections.Generic;

namespace Dominio
{
    public class LineaDeVenta
    {
        public List<Producto> Productos { get; }
        public int Cantidad { get; set; }

        public LineaDeVenta(Producto producto, int cantidad)
        {
            Productos = new List<Producto> {producto};
            Cantidad = cantidad;
        }

        public void AgregarProducto(Producto producto)
        {
            Productos.Add(producto);
        }

        public double GetTotal()
        {
            double total = 0;
            foreach(Producto articulo in Productos)
            {
                total += articulo.Precio;
            }
            return total * Cantidad;
        }

        public void RemoverAgregado(Producto agregado)
        {
            Productos.Remove(Productos.Find(art => art.Codigo == agregado.Codigo));
        }
    }
}