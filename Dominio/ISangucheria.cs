﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public interface ISangucheria
    {
        List<Producto> ObtenerAgregados();
        Producto ObtenerAgregadoPor(string descripcion);
        List<Producto> ObtenerProductosPor(Rubro rubro);
        Cliente ObtenerClientePorDefecto();
        CondicionTributaria ObtenerCondicionTributaria();
        List<Empleado> ObtenerEmpleados();
        void AgregarVentaAlTurnoEnCurso(Venta venta);
        void IniciarTurno(Empleado empleado);
        double FinalizarTurnoEnCurso();
        bool ComprobarExistencia(LineaDeVenta linea);
        void ActualizarExistenciaPor(LineaDeVenta linea);
    }
}
