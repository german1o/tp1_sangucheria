﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aplicacion;
using Dominio;

namespace Servicios
{
    public class AdaptadorConexionAfip : IAfip
    {
        private int _numeroComprobante;

        public int ObtenerUltimoNumeroDeComprobante()
        {
            return ++_numeroComprobante;
        }

        public string AutorizarVenta(Venta venta)
        {
            int cae = new Random().Next(50);
            venta.Autorizar(cae);
            return "Venta autorizada con exito - Cae: " + cae;
        }

    }
}
