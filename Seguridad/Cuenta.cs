﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio;

namespace Seguridad
{
    public class Cuenta
    {
        public string Usuario { get; }
        public string Contraseña { get; }
        public Empleado Empleado {get;}

        public Cuenta(string usuario, string contraseña, Empleado empleado)
        {
            Usuario = usuario;
            Contraseña = contraseña;
            Empleado = empleado;
        }
    }
}
