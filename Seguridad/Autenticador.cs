﻿using Dominio;


namespace Seguridad
{
    public class Autenticador
    {
        private readonly ICuentas _cuentas;

        public Autenticador(ICuentas cuentas)
        {
            _cuentas = cuentas;
        }

        public Empleado AutenticarUsuario(string usuario, string contraseña)
        {
            return _cuentas.ObtenerCuenta(usuario, contraseña).Empleado;
        }
    }
}
