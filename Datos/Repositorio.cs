﻿using Dominio;
using Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class Repositorio : ISangucheria, ICuentas
    {
        private readonly PuntoDeVenta _pdv;
        private readonly Cliente _clientePorDefecto;
        private readonly List<Cuenta> _cuentas;
        private readonly List<Empleado> _empleados;
        private readonly List<Producto> _productos;

        public Repositorio()
        {
            _pdv = new PuntoDeVenta(1,new List<Turno>());
            _clientePorDefecto = new Cliente("Donald Trump", 1234556, CondicionTributaria.ConsumidorFinal);
            _cuentas = new List<Cuenta>();
            _empleados = new List<Empleado>();
            _productos = new List<Producto>();

            Iniciar();
        }

        public void Iniciar()
        {
            _empleados.Add(new Empleado("Kim", "Jong-un"));
            _empleados.Add(new Empleado("Vladimir", "Putin"));
            _empleados.Add(new Empleado("Miauricio", "Mcree"));

            _cuentas.Add(new Cuenta("coreanito", "1234", _empleados[0]));
            _cuentas.Add(new Cuenta("rrruso", "4321", _empleados[1]));
            _cuentas.Add(new Cuenta("miauuu", "1111", _empleados[2]));

            _productos.Add(new Producto(1, 70.0, "Milanesa", 5, Rubro.Sanguches, true));
            _productos.Add(new Producto(2, 85.0, "Milanesa especial", 5, Rubro.Sanguches, true));       
            _productos.Add(new Producto(3, 65.0, "Hamburguesa", 2, Rubro.Sanguches, true));
            _productos.Add(new Producto(4, 80.0, "Hamburguesa especial", 3, Rubro.Sanguches, true));
            _productos.Add(new Producto(5, 75.0, "Lomito", 1, Rubro.Sanguches, true));
            _productos.Add(new Producto(6, 85.0, "Lomito especial", 0, Rubro.Sanguches, true));

            _productos.Add(new Producto(7, 60.0, "Coca-Cola 1.5L", 10, Rubro.Bebidas, true));
            _productos.Add(new Producto(8, 40.0, "Levite Manzana 1.5L", 0, Rubro.Bebidas,false));
            _productos.Add(new Producto(9, 70.0, "Quilmes Cristal 1L", 7, Rubro.Bebidas,true));
            _productos.Add(new Producto(10, 35.0, "Agua Mineral 1.5L", 0, Rubro.Bebidas, false));

            _productos.Add(new Producto(1, 0.0, "Tomate", 0,Rubro.Agregados, false));
            _productos.Add(new Producto(2, 0.0, "Lechuga", 0,Rubro.Agregados, false));
            _productos.Add(new Producto(3, 0.0, "Aji", 0,Rubro.Agregados, false));
            _productos.Add(new Producto(4, 15.0, "Jamon", 0,Rubro.Agregados, false));
            _productos.Add(new Producto(5, 15.0, "Queso", 0,Rubro.Agregados, false));
            _productos.Add(new Producto(6, 15.0, "Huevo", 0,Rubro.Agregados, false));
        }

        public List<Producto> ObtenerProductosPor(Rubro rubro)
        {
            List<Producto> productos = new List<Producto>();

            foreach (Producto p in _productos)
            {
                if (p.Rubro == rubro)
                {
                    productos.Add(p);
                }
            }

            return productos;
        }

        public List<Producto> ObtenerAgregados()
        {
            return _productos.FindAll(producto => producto.Rubro == Rubro.Agregados);
        }

        public Cuenta ObtenerCuenta(string usuario, string contraseña)
        {
            Cuenta cuenta = null;

            foreach (Cuenta c in _cuentas)
            {
                if (c.Usuario == usuario && c.Contraseña == contraseña)
                    cuenta = c;
            }

            if (cuenta == null)
            {
                throw new ExcepcionCuentaNoEncontrada();
            }
            
            return cuenta;
        }

        public Producto ObtenerAgregadoPor(string descripcion)
        {
            return ObtenerAgregados().Find(agregado => agregado.Descripcion == descripcion);
        }

        public Cliente ObtenerClientePorDefecto()
        {
            return _clientePorDefecto;
        }


        public CondicionTributaria ObtenerCondicionTributaria()
        {
            return CondicionTributaria.Monotributista;
        }

        public List<Empleado> ObtenerEmpleados()
        {
            return _empleados;
        }

        public void AgregarVentaAlTurnoEnCurso(Venta venta)
        {
            _pdv.AgregarVentaAlTurnoEnCurso(venta);
        }


        public void IniciarTurno(Empleado empleado)
        {
            _pdv.IniciarTurno(empleado);
        }

        public double FinalizarTurnoEnCurso()
        {
            return _pdv.FinalizarTurnoEnCurso();
        }

        public bool ComprobarExistencia(LineaDeVenta linea)
        {
            foreach (var producto in linea.Productos)
            {
                if (!producto.SeControlanExistencias) continue;
                if (producto.Existencias < linea.Cantidad)
                {
                    return false;
                }
            }

            return true;

        }

        public void ActualizarExistenciaPor(LineaDeVenta linea)
        {
            foreach (var producto in linea.Productos)
            {
                if (!producto.SeControlanExistencias) continue;
                _productos.Find(p => p == producto).Existencias -= linea.Cantidad;
            }
        }

    }
}
