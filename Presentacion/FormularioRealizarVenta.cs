﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Aplicacion;
using Datos;
using Dominio;
using Servicios;

namespace Presentacion
{
    public partial class FormularioRealizarVenta : Form, IVistaRegistrarVenta
    {
        private readonly ControladorRegistrarVenta _controlador;
        

        public FormularioRealizarVenta(Empleado empleado)
        {
            InitializeComponent();
            _controlador = new ControladorRegistrarVenta(this, new Repositorio(), new AdaptadorConexionAfip());
            _controlador.Iniciar(empleado);
            Inicializar();
        }

        private void Inicializar()
        {
            FormBorderStyle = FormBorderStyle.None;
            List<string> rubros = Enum.GetNames(typeof(Rubro)).ToList();
            rubros.Remove("Agregados");
            comboBoxRubro.DataSource = rubros;
            textFieldCantidad.TextChanged += TextFieldCantidadOnTextChanged;
            BotonConfirmarVenta.Enabled = false;
            Limpiar();
        }


        private void FormularioMenuPrincipal_Load(object sender, EventArgs e)
        {
            CenterToScreen();
        }

        private void comboBoxRubro_SelectionChangeCommitted(object sender, EventArgs e)
        {
            
            productoBindingSource.Clear();
            _controlador.RubroSeleccionado(comboBoxRubro.SelectedValue.ToString());
            

        }

        public void CargarProductos(List<Producto> productos)
        {
            foreach (Producto producto in productos)
            {
                productoBindingSource.Add(producto);
                if (!producto.SeControlanExistencias)
                {
                    dataGridProductos.Rows[dataGridProductos.RowCount - 1].Cells[2].Value = "-";
                }
                else
                {
                    dataGridProductos.Rows[dataGridProductos.RowCount - 1].Cells[2].Value = producto.Existencias;
                }

            }
            dataGridProductos.ClearSelection();
            groupBoxAgregados.Visible = false;
        }
        
        public void CargarAgregados(List<Producto> agregados)
        {
            groupBoxAgregados.Visible = true;
            foreach(Producto agregado in agregados)
            {
                CheckBox cb = new CheckBox {Name = agregado.Descripcion, Text = agregado.Descripcion + "- $" + agregado.Precio , Dock = DockStyle.Top};
                cb.CheckedChanged += checkBoxAgregado_CheckedChanged;
                cb.CheckedChanged += _controlador.ActualizarTotalLinea;
                if (agregado.Precio <= 0)
                {
                    cb.Checked = true;
                }
                
                groupBoxAgregados.Controls.Add(cb);
            }
            
        }

        private void checkBoxAgregado_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox) sender;
            
            if (cb.Checked)
            {
                _controlador.AñadirAgregado(cb.Name);
            }
            else
            {
                _controlador.RemoverAgregado(cb.Name);
            }

        }

        public void ActualizarTotalLinea(double total)
        {
            labelTotalLinea.Text = "Subtotal: $" + total.ToString(CultureInfo.CurrentCulture);
        }

        public void AgregarLineaDeVentaALista(LineaDeVenta linea)
        {
            string articulos = "";
            lineaDeVentaBindingSource.Add(linea);
            foreach (Producto articulo in linea.Productos)
            {
                articulos += articulo.Descripcion + ", ";
            }
            articulos = articulos.Substring(0, articulos.Length - 2);
            TablaDeLineasDeVenta.Rows[TablaDeLineasDeVenta.RowCount-1].Cells[1].Value = articulos;
            TablaDeLineasDeVenta.Rows[TablaDeLineasDeVenta.RowCount - 1].Cells[2].Value = linea.GetTotal();

            BotonConfirmarVenta.Enabled = true;

        }

        public void ActualizarTotalVenta(double total)
        {
            TextoTotalAPagar.Text = "Total a pagar: $" + total.ToString(CultureInfo.CurrentCulture);
        }

        private void TextFieldCantidadOnTextChanged(object sender, EventArgs e)
        {
            if (textFieldCantidad.Text == string.Empty) return;
            _controlador.ActualizarCantidad(int.Parse(textFieldCantidad.Text));
            _controlador.ActualizarTotalLinea(this, EventArgs.Empty);      
        }

        private void dataGridProductos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            groupBoxAgregados.Controls.Clear();
            _controlador.SeleccionarProducto((Producto)dataGridProductos.SelectedRows[0].DataBoundItem, int.Parse(textFieldCantidad.Text));
            _controlador.ActualizarTotalLinea(this, EventArgs.Empty);
            BotonAgregarLineaDeVenta.Visible = true;
            labelTotalLinea.Visible = true;
            labelCantidad.Visible = true;
            textFieldCantidad.Visible = true;
        }

        private void BotonAgregarLineaDeVenta_Click(object sender, EventArgs e)
        {
            _controlador.AgregarLineaDeVenta();
            //_controlador.RubroSeleccionado(comboBoxRubro.SelectedValue.ToString());
        }

        private void BotonConfirmarVenta_Click(object sender, EventArgs e)
        {
            _controlador.ConfirmarVenta();
        }

        public void Cerrar()
        {
            new FormularioIniciarSesion().Show();
            Close();
        }

        public void Limpiar()
        {
            productoBindingSource.Clear();
            comboBoxRubro.SelectedIndex = -1;
            textFieldCantidad.Text = "1";
            labelTotalLinea.Text = "Subtotal: $$$";

            labelCantidad.Visible = false;
            labelTotalLinea.Visible = false;
            groupBoxAgregados.Visible = false;
            BotonAgregarLineaDeVenta.Visible = false;
            textFieldCantidad.Visible = false;
        }

        public void MostrarMensaje(string mensaje)
        {
            MessageBox.Show(mensaje);
        }

        public void RestaurarVista()
        {
            TextoTotalAPagar.Text = "Total a pagar: $$$";

            // Limpiar la tabla de lineas de venta
            lineaDeVentaBindingSource.Clear();
            BotonConfirmarVenta.Enabled = false;

        }

        private void finalizarTurnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _controlador.FinalizarTurno();
        }
    }
}
