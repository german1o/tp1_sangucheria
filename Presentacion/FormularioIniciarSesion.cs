﻿using System;
using System.Windows.Forms;
using Aplicacion;
using Datos;
using Dominio;

namespace Presentacion
{
    public partial class FormularioIniciarSesion : Form , ILogin
    {
        private ControladorIniciarSesion _controlador;

        public FormularioIniciarSesion()
        {
            _controlador = new ControladorIniciarSesion(this, new Repositorio());
            InitializeComponent();
            Closed += Exit;
        }

        private void Exit(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void botonEntrar_click(object sender, EventArgs e)
        {
            _controlador.IniciarSesion(campoUsuario.Text, campoContraseña.Text);
        }

        private void FormularioIniciarSesion_Load(object sender, EventArgs e)
        {
            CenterToScreen();
        }

        public void MostrarMensaje(string mensaje)
        {
            MessageBox.Show(mensaje);
        }

        public void IniciarSesionPor(Empleado empleado)
        {
            new FormularioRealizarVenta(empleado).Show();
            Dispose();
        }
    }
}
