﻿namespace Presentacion
{
    partial class FormularioRealizarVenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.BotonConfirmarVenta = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.TablaDeLineasDeVenta = new System.Windows.Forms.DataGridView();
            this.cantidadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Articulos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Subtotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lineaDeVentaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.dataGridProductos = new System.Windows.Forms.DataGridView();
            this.productoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.labelCantidad = new System.Windows.Forms.Label();
            this.textFieldCantidad = new System.Windows.Forms.TextBox();
            this.BotonAgregarLineaDeVenta = new System.Windows.Forms.Button();
            this.TextoTotalAPagar = new System.Windows.Forms.Label();
            this.labelTotalLinea = new System.Windows.Forms.Label();
            this.MenuDeOpciones = new System.Windows.Forms.MenuStrip();
            this.historialesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.finalizarTurnoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comboBoxRubro = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxAgregados = new System.Windows.Forms.GroupBox();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.existenciasDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.TablaDeLineasDeVenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lineaDeVentaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProductos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productoBindingSource)).BeginInit();
            this.MenuDeOpciones.SuspendLayout();
            this.SuspendLayout();
            // 
            // BotonConfirmarVenta
            // 
            this.BotonConfirmarVenta.Location = new System.Drawing.Point(918, 462);
            this.BotonConfirmarVenta.Name = "BotonConfirmarVenta";
            this.BotonConfirmarVenta.Size = new System.Drawing.Size(124, 28);
            this.BotonConfirmarVenta.TabIndex = 2;
            this.BotonConfirmarVenta.Text = "Confirmar Venta";
            this.BotonConfirmarVenta.UseVisualStyleBackColor = true;
            this.BotonConfirmarVenta.Click += new System.EventHandler(this.BotonConfirmarVenta_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(442, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Lineas de venta";
            // 
            // TablaDeLineasDeVenta
            // 
            this.TablaDeLineasDeVenta.AllowUserToAddRows = false;
            this.TablaDeLineasDeVenta.AllowUserToDeleteRows = false;
            this.TablaDeLineasDeVenta.AllowUserToResizeColumns = false;
            this.TablaDeLineasDeVenta.AllowUserToResizeRows = false;
            this.TablaDeLineasDeVenta.AutoGenerateColumns = false;
            this.TablaDeLineasDeVenta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TablaDeLineasDeVenta.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cantidadDataGridViewTextBoxColumn,
            this.Articulos,
            this.Subtotal});
            this.TablaDeLineasDeVenta.DataSource = this.lineaDeVentaBindingSource;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TablaDeLineasDeVenta.DefaultCellStyle = dataGridViewCellStyle1;
            this.TablaDeLineasDeVenta.Location = new System.Drawing.Point(445, 60);
            this.TablaDeLineasDeVenta.Name = "TablaDeLineasDeVenta";
            this.TablaDeLineasDeVenta.ReadOnly = true;
            this.TablaDeLineasDeVenta.RowHeadersVisible = false;
            this.TablaDeLineasDeVenta.RowHeadersWidth = 80;
            this.TablaDeLineasDeVenta.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.TablaDeLineasDeVenta.RowTemplate.Height = 44;
            this.TablaDeLineasDeVenta.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TablaDeLineasDeVenta.Size = new System.Drawing.Size(597, 364);
            this.TablaDeLineasDeVenta.TabIndex = 7;
            // 
            // cantidadDataGridViewTextBoxColumn
            // 
            this.cantidadDataGridViewTextBoxColumn.DataPropertyName = "Cantidad";
            this.cantidadDataGridViewTextBoxColumn.HeaderText = "Cantidad";
            this.cantidadDataGridViewTextBoxColumn.Name = "cantidadDataGridViewTextBoxColumn";
            this.cantidadDataGridViewTextBoxColumn.ReadOnly = true;
            this.cantidadDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.cantidadDataGridViewTextBoxColumn.Width = 70;
            // 
            // Articulos
            // 
            this.Articulos.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Articulos.HeaderText = "Productos";
            this.Articulos.Name = "Articulos";
            this.Articulos.ReadOnly = true;
            this.Articulos.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Subtotal
            // 
            this.Subtotal.HeaderText = "Subtotal";
            this.Subtotal.Name = "Subtotal";
            this.Subtotal.ReadOnly = true;
            this.Subtotal.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // lineaDeVentaBindingSource
            // 
            this.lineaDeVentaBindingSource.DataSource = typeof(Dominio.LineaDeVenta);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(155, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "Nueva linea de venta";
            // 
            // dataGridProductos
            // 
            this.dataGridProductos.AllowUserToAddRows = false;
            this.dataGridProductos.AllowUserToDeleteRows = false;
            this.dataGridProductos.AllowUserToResizeColumns = false;
            this.dataGridProductos.AllowUserToResizeRows = false;
            this.dataGridProductos.AutoGenerateColumns = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridProductos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridProductos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn2,
            this.existenciasDataGridViewTextBoxColumn});
            this.dataGridProductos.DataSource = this.productoBindingSource;
            this.dataGridProductos.Location = new System.Drawing.Point(12, 90);
            this.dataGridProductos.MultiSelect = false;
            this.dataGridProductos.Name = "dataGridProductos";
            this.dataGridProductos.ReadOnly = true;
            this.dataGridProductos.RowHeadersVisible = false;
            this.dataGridProductos.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridProductos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridProductos.Size = new System.Drawing.Size(399, 276);
            this.dataGridProductos.TabIndex = 10;
            this.dataGridProductos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridProductos_CellClick);
            // 
            // productoBindingSource
            // 
            this.productoBindingSource.DataSource = typeof(Dominio.Producto);
            // 
            // labelCantidad
            // 
            this.labelCantidad.AutoSize = true;
            this.labelCantidad.Location = new System.Drawing.Point(175, 374);
            this.labelCantidad.Name = "labelCantidad";
            this.labelCantidad.Size = new System.Drawing.Size(62, 16);
            this.labelCantidad.TabIndex = 11;
            this.labelCantidad.Text = "Cantidad";
            // 
            // textFieldCantidad
            // 
            this.textFieldCantidad.Location = new System.Drawing.Point(243, 372);
            this.textFieldCantidad.Name = "textFieldCantidad";
            this.textFieldCantidad.Size = new System.Drawing.Size(40, 22);
            this.textFieldCantidad.TabIndex = 12;
            this.textFieldCantidad.Text = "1";
            // 
            // BotonAgregarLineaDeVenta
            // 
            this.BotonAgregarLineaDeVenta.Location = new System.Drawing.Point(178, 429);
            this.BotonAgregarLineaDeVenta.Name = "BotonAgregarLineaDeVenta";
            this.BotonAgregarLineaDeVenta.Size = new System.Drawing.Size(166, 35);
            this.BotonAgregarLineaDeVenta.TabIndex = 13;
            this.BotonAgregarLineaDeVenta.Text = "Agregar Línea de Venta";
            this.BotonAgregarLineaDeVenta.UseVisualStyleBackColor = true;
            this.BotonAgregarLineaDeVenta.Click += new System.EventHandler(this.BotonAgregarLineaDeVenta_Click);
            // 
            // TextoTotalAPagar
            // 
            this.TextoTotalAPagar.AutoSize = true;
            this.TextoTotalAPagar.Location = new System.Drawing.Point(926, 434);
            this.TextoTotalAPagar.Name = "TextoTotalAPagar";
            this.TextoTotalAPagar.Size = new System.Drawing.Size(116, 16);
            this.TextoTotalAPagar.TabIndex = 16;
            this.TextoTotalAPagar.Text = "Total a pagar: $$$";
            // 
            // labelTotalLinea
            // 
            this.labelTotalLinea.AutoSize = true;
            this.labelTotalLinea.Location = new System.Drawing.Point(175, 403);
            this.labelTotalLinea.Name = "labelTotalLinea";
            this.labelTotalLinea.Size = new System.Drawing.Size(84, 16);
            this.labelTotalLinea.TabIndex = 17;
            this.labelTotalLinea.Text = "Subtotal: $$$";
            // 
            // MenuDeOpciones
            // 
            this.MenuDeOpciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuDeOpciones.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.historialesToolStripMenuItem,
            this.ayudaToolStripMenuItem});
            this.MenuDeOpciones.Location = new System.Drawing.Point(0, 0);
            this.MenuDeOpciones.Name = "MenuDeOpciones";
            this.MenuDeOpciones.Size = new System.Drawing.Size(1088, 24);
            this.MenuDeOpciones.TabIndex = 19;
            this.MenuDeOpciones.Text = "menuStrip1";
            // 
            // historialesToolStripMenuItem
            // 
            this.historialesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.finalizarTurnoToolStripMenuItem});
            this.historialesToolStripMenuItem.Name = "historialesToolStripMenuItem";
            this.historialesToolStripMenuItem.Size = new System.Drawing.Size(131, 20);
            this.historialesToolStripMenuItem.Text = "Menú de opciones";
            // 
            // finalizarTurnoToolStripMenuItem
            // 
            this.finalizarTurnoToolStripMenuItem.Name = "finalizarTurnoToolStripMenuItem";
            this.finalizarTurnoToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.finalizarTurnoToolStripMenuItem.Text = "Finalizar Turno";
            this.finalizarTurnoToolStripMenuItem.Click += new System.EventHandler(this.finalizarTurnoToolStripMenuItem_Click);
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.ayudaToolStripMenuItem.Text = "Ayuda";
            // 
            // comboBoxRubro
            // 
            this.comboBoxRubro.FormattingEnabled = true;
            this.comboBoxRubro.Location = new System.Drawing.Point(64, 60);
            this.comboBoxRubro.Name = "comboBoxRubro";
            this.comboBoxRubro.Size = new System.Drawing.Size(223, 24);
            this.comboBoxRubro.TabIndex = 22;
            this.comboBoxRubro.Text = "Seleccione el rubro del producto";
            this.comboBoxRubro.SelectionChangeCommitted += new System.EventHandler(this.comboBoxRubro_SelectionChangeCommitted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 16);
            this.label1.TabIndex = 23;
            this.label1.Text = "Rubro";
            // 
            // groupBoxAgregados
            // 
            this.groupBoxAgregados.Location = new System.Drawing.Point(12, 372);
            this.groupBoxAgregados.Name = "groupBoxAgregados";
            this.groupBoxAgregados.Size = new System.Drawing.Size(139, 203);
            this.groupBoxAgregados.TabIndex = 24;
            this.groupBoxAgregados.TabStop = false;
            this.groupBoxAgregados.Text = "Agregados";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Descripcion";
            this.dataGridViewTextBoxColumn3.HeaderText = "Descripcion";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Precio";
            this.dataGridViewTextBoxColumn2.HeaderText = "Precio";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 75;
            // 
            // existenciasDataGridViewTextBoxColumn
            // 
            this.existenciasDataGridViewTextBoxColumn.HeaderText = "Existencias";
            this.existenciasDataGridViewTextBoxColumn.Name = "existenciasDataGridViewTextBoxColumn";
            this.existenciasDataGridViewTextBoxColumn.ReadOnly = true;
            this.existenciasDataGridViewTextBoxColumn.Width = 75;
            // 
            // FormularioRealizarVenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1088, 584);
            this.Controls.Add(this.groupBoxAgregados);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxRubro);
            this.Controls.Add(this.MenuDeOpciones);
            this.Controls.Add(this.labelTotalLinea);
            this.Controls.Add(this.TextoTotalAPagar);
            this.Controls.Add(this.BotonAgregarLineaDeVenta);
            this.Controls.Add(this.textFieldCantidad);
            this.Controls.Add(this.labelCantidad);
            this.Controls.Add(this.dataGridProductos);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TablaDeLineasDeVenta);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.BotonConfirmarVenta);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.MenuDeOpciones;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormularioRealizarVenta";
            this.Text = "PDV";
            this.Load += new System.EventHandler(this.FormularioMenuPrincipal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TablaDeLineasDeVenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lineaDeVentaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProductos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productoBindingSource)).EndInit();
            this.MenuDeOpciones.ResumeLayout(false);
            this.MenuDeOpciones.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button BotonConfirmarVenta;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView TablaDeLineasDeVenta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dataGridProductos;
        private System.Windows.Forms.Label labelCantidad;
        private System.Windows.Forms.TextBox textFieldCantidad;
        private System.Windows.Forms.Button BotonAgregarLineaDeVenta;
        private System.Windows.Forms.Label TextoTotalAPagar;
        private System.Windows.Forms.Label labelTotalLinea;
        private System.Windows.Forms.MenuStrip MenuDeOpciones;
        private System.Windows.Forms.ToolStripMenuItem historialesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource productoBindingSource;
        private System.Windows.Forms.ComboBox comboBoxRubro;
        private System.Windows.Forms.GroupBox groupBoxAgregados;
        private System.Windows.Forms.BindingSource lineaDeVentaBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Articulos;
        private System.Windows.Forms.DataGridViewTextBoxColumn Subtotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descripcionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn precioDataGridViewTextBoxColumn;
        private System.Windows.Forms.ToolStripMenuItem finalizarTurnoToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn existenciasDataGridViewTextBoxColumn;
    }
}