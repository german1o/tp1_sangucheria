﻿using Dominio;

namespace Aplicacion
{
    public interface IAfip
    {
        int ObtenerUltimoNumeroDeComprobante();
        string AutorizarVenta(Venta venta);
    }
}
