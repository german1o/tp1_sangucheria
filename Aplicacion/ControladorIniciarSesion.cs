﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio;
using Seguridad;

namespace Aplicacion
{
    public class ControladorIniciarSesion
    {
        private readonly ILogin _vista;
        private readonly Autenticador _autenticador;

        public ControladorIniciarSesion(ILogin vista, ICuentas cuentas)
        {
            _vista = vista;
            _autenticador = new Autenticador(cuentas);
        }

        public void IniciarSesion(string usuario, string contraseña)
        {
            try
            {
                _vista.IniciarSesionPor(_autenticador.AutenticarUsuario(usuario, contraseña));
            }
            catch (ExcepcionCuentaNoEncontrada e)
            {
                _vista.MostrarMensaje(e.Message);
            }
           
        }
    }
}
