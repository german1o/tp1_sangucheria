﻿using Dominio;
using System;
using System.Collections.Generic;

namespace Aplicacion
{
    public class ControladorRegistrarVenta
    {
        private readonly ISangucheria _sangucheria;
        private readonly IVistaRegistrarVenta _vista;
        private LineaDeVenta _linea;
        private Venta _venta;
        private readonly IAfip _afip;


        public ControladorRegistrarVenta(IVistaRegistrarVenta vista, ISangucheria sangucheria, IAfip afip)
        {
            //CORREGIR LO DEL EMPLEADO (sesion)
            _vista = vista;
            _sangucheria = sangucheria;
            _afip = afip;
        }

        public void Iniciar(Empleado empleado)
        {
            _sangucheria.IniciarTurno(empleado);

            _venta = new Venta(
                _sangucheria.ObtenerClientePorDefecto(),
                _sangucheria.ObtenerCondicionTributaria(),
                _afip.ObtenerUltimoNumeroDeComprobante()
            );
        }

        public void SeleccionarProducto(Producto producto, int cantidad)
        {
            _linea = new LineaDeVenta(producto, cantidad);

            if (producto.Rubro == Rubro.Sanguches)
            {
                _vista.CargarAgregados(_sangucheria.ObtenerAgregados());
            }
        }

        public void ActualizarTotalLinea(object sender, EventArgs e)
        {
            _vista.ActualizarTotalLinea(_linea.GetTotal());
        }

        public void AñadirAgregado(string descripcion)
        {
            _linea.AgregarProducto(_sangucheria.ObtenerAgregadoPor(descripcion));
        }

        public void RemoverAgregado(string descripcion)
        {
            _linea.RemoverAgregado(_sangucheria.ObtenerAgregadoPor(descripcion));
        }

        public void ActualizarCantidad(int cantidad)
        {
            _linea.Cantidad = cantidad;
        }

        public void AgregarLineaDeVenta()
        {
            if (!_sangucheria.ComprobarExistencia(_linea))
            {
                _vista.MostrarMensaje("Uno de los productos seleccionados no posee existencias disponibles para la cantidad indicada.");
                return;
            }
            _venta.AgregarLineaDeVenta(_linea);
            _sangucheria.ActualizarExistenciaPor(_linea);
            _vista.AgregarLineaDeVentaALista(_linea);
            _vista.ActualizarTotalVenta(_venta.GetTotal());
            _vista.Limpiar();
        }

        public void ConfirmarVenta()
        {
            _vista.MostrarMensaje(_afip.AutorizarVenta(_venta));
            _sangucheria.AgregarVentaAlTurnoEnCurso(_venta);
            
            _vista.RestaurarVista();

            _venta = new Venta(
                _sangucheria.ObtenerClientePorDefecto(),
                _sangucheria.ObtenerCondicionTributaria(),
                _afip.ObtenerUltimoNumeroDeComprobante()
            );
        }

        public void FinalizarTurno()
        {
            string mensaje = "Total recaudado en el turno: " + _sangucheria.FinalizarTurnoEnCurso();
            _vista.MostrarMensaje(mensaje);
            _vista.Cerrar();
        }

        public void RubroSeleccionado(string rubro)
        {
            Enum.TryParse(rubro, out Rubro rubroProductos);
            _vista.CargarProductos(_sangucheria.ObtenerProductosPor(rubroProductos));

        }
    }
}
