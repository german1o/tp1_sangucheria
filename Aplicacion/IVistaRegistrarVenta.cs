﻿using System.Collections.Generic;
using Dominio;

namespace Aplicacion
{
    public interface IVistaRegistrarVenta
    {
        void CargarAgregados(List<Producto> agregados);
        void CargarProductos(List<Producto> productos);
        void ActualizarTotalLinea(double total);
        void AgregarLineaDeVentaALista(LineaDeVenta linea);
        void ActualizarTotalVenta(double total);
        void RestaurarVista();
        void MostrarMensaje(string mensaje);
        void Cerrar();
        void Limpiar();
    }
}
