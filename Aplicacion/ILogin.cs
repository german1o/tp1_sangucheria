﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio;

namespace Aplicacion
{
    public interface ILogin
    {
        void MostrarMensaje(string mensaje);
        void IniciarSesionPor(Empleado empleado);
    }
}
