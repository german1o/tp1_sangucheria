﻿using System.Collections.Generic;
using Datos;
using Dominio;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace Pruebas
{
    [TestFixture]
    public class Pruebas
    {
        private readonly Repositorio _sangucheria = new Repositorio();


        [Test]
        public void CuandoSePiden2ProductosIgualesElPrecioEs()
        {
            List<Producto> productos = _sangucheria.ObtenerProductosPor(Rubro.Sanguches);
            Producto sanguche = productos.Find(producto => producto.Descripcion == "Milanesa");

            LineaDeVenta ldv = new LineaDeVenta(sanguche, 2);


            Assert.AreEqual(ldv.GetTotal(), 140.0);
        }

        [Test]
        public void CuandoSePideUnProductoConProductoExtraElPrecioEs()
        {
            List<Producto> productos = _sangucheria.ObtenerProductosPor(Rubro.Sanguches);
            Producto sanguche = productos.Find(producto => producto.Descripcion == "Milanesa");
            List<Producto> agregados = _sangucheria.ObtenerAgregados();
            Producto agregado = agregados.Find(producto => producto.Descripcion == "Jamon");

            LineaDeVenta ldv = new LineaDeVenta(sanguche, 1);
            ldv.AgregarProducto(agregado);


            Assert.AreEqual(ldv.GetTotal(), 85.0);
        }

        [Test]
        public void CuandoSePidenDosProductosDiferentesElPrecioEs()
        {
            Venta venta = new Venta(_sangucheria.ObtenerClientePorDefecto(), _sangucheria.ObtenerCondicionTributaria(), 1);
            List<Producto> productos = _sangucheria.ObtenerProductosPor(Rubro.Sanguches);
            Producto sanguche1 = productos.Find(producto => producto.Descripcion == "Milanesa");
            Producto sanguche2 = productos.Find(producto => producto.Descripcion == "Milanesa especial");

            LineaDeVenta ldv1 = new LineaDeVenta(sanguche1, 2);
            venta.AgregarLineaDeVenta(ldv1);
            LineaDeVenta ldv2 = new LineaDeVenta(sanguche2, 3);
            venta.AgregarLineaDeVenta(ldv2);


            Assert.AreEqual(venta.GetTotal(), 395.0);
        }

        [Test]
        public void CuandoSeAgregaUnProductoSinExistenciasSuficientes()
        {
            Venta venta = new Venta(_sangucheria.ObtenerClientePorDefecto(), _sangucheria.ObtenerCondicionTributaria(), 1);
            List<Producto> productos = _sangucheria.ObtenerProductosPor(Rubro.Sanguches);
            Producto sanguche = productos.Find(producto => producto.Descripcion == "Lomito especial");

            LineaDeVenta ldv = new LineaDeVenta(sanguche, 1);
            if (_sangucheria.ComprobarExistencia(ldv))
            {
                venta.AgregarLineaDeVenta(ldv);
            }

            Assert.AreEqual(venta.GetTotal(), 0.0);
        }

        [Test]
        public void CuandoSePideElArqueoDeCajaDelTurno()
        {
            _sangucheria.IniciarTurno(_sangucheria.ObtenerEmpleados()[1]);
            Venta venta1 = new Venta(_sangucheria.ObtenerClientePorDefecto(), _sangucheria.ObtenerCondicionTributaria(), 1);
            Venta venta2 = new Venta(_sangucheria.ObtenerClientePorDefecto(), _sangucheria.ObtenerCondicionTributaria(), 2);
            List<Producto> productos = _sangucheria.ObtenerProductosPor(Rubro.Sanguches);
            Producto sanguche1 = productos.Find(producto => producto.Descripcion == "Milanesa");
            Producto sanguche2 = productos.Find(producto => producto.Descripcion == "Milanesa especial");

            LineaDeVenta ldv1 = new LineaDeVenta(sanguche1, 2);
            venta1.AgregarLineaDeVenta(ldv1);
            venta2.AgregarLineaDeVenta(ldv1);
            LineaDeVenta ldv2 = new LineaDeVenta(sanguche2, 3);
            venta1.AgregarLineaDeVenta(ldv2);
            venta2.AgregarLineaDeVenta(ldv2);
            _sangucheria.AgregarVentaAlTurnoEnCurso(venta1);
            _sangucheria.AgregarVentaAlTurnoEnCurso(venta2);

            

            Assert.AreEqual(_sangucheria.FinalizarTurnoEnCurso(), 790.0);
        }

        
    }
}
